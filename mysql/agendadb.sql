-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Jan-2019 às 15:13
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agendadb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendapessoas`
--

CREATE TABLE `agendapessoas` (
  `id_agenda_pessoas` int(10) UNSIGNED NOT NULL,
  `id_pessoas` int(10) UNSIGNED NOT NULL,
  `id_reuniao` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_01_09_164157_cria_tabela_pessoas', 1),
(2, '2019_01_09_173250_cria_tabela_reuniao', 1),
(3, '2019_01_09_174349_cria_tabela_agendapessoas', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id_pessoas` int(3) UNSIGNED NOT NULL,
  `nome` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `pessoas`
--

INSERT INTO `pessoas` (`id_pessoas`, `nome`, `email`, `telefone`, `created_at`, `updated_at`) VALUES
(1, 'Patrick', 'patrick.pacifico.s@gmail.com', '21983388144', '2019-01-09 19:26:25', '2019-01-11 05:26:13'),
(2, 'Fernando', 'fernando.silva@applus.com', '31997397881', '2019-01-09 19:27:14', '2019-01-09 19:27:14'),
(3, 'Applus', 'info@applus.com.br', '2112345678', '2019-01-09 20:49:21', '2019-01-09 20:49:21'),
(4, 'João', 'jp2000@hotmail.com', '21971246669', '2019-01-10 05:52:12', '2019-01-10 05:52:12'),
(5, 'Joana', 'joana92@yahoo.com.br', '21974586522', '2019-01-11 04:51:28', '2019-01-11 04:51:28'),
(7, 'Caetano', 'caet54@gmail.com', '2187455244', '2019-01-11 06:01:27', '2019-01-11 06:01:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reuniao`
--

CREATE TABLE `reuniao` (
  `id_reuniao` int(3) UNSIGNED NOT NULL,
  `dt_reuniao` date NOT NULL,
  `hr_reuniao` time NOT NULL,
  `oberservacao` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agendapessoas`
--
ALTER TABLE `agendapessoas`
  ADD PRIMARY KEY (`id_agenda_pessoas`),
  ADD KEY `agendapessoas_id_pessoas_foreign` (`id_pessoas`),
  ADD KEY `agendapessoas_id_reuniao_foreign` (`id_reuniao`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id_pessoas`);

--
-- Indexes for table `reuniao`
--
ALTER TABLE `reuniao`
  ADD PRIMARY KEY (`id_reuniao`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agendapessoas`
--
ALTER TABLE `agendapessoas`
  MODIFY `id_agenda_pessoas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id_pessoas` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reuniao`
--
ALTER TABLE `reuniao`
  MODIFY `id_reuniao` int(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `agendapessoas`
--
ALTER TABLE `agendapessoas`
  ADD CONSTRAINT `agendapessoas_id_pessoas_foreign` FOREIGN KEY (`id_pessoas`) REFERENCES `pessoas` (`id_pessoas`),
  ADD CONSTRAINT `agendapessoas_id_reuniao_foreign` FOREIGN KEY (`id_reuniao`) REFERENCES `reuniao` (`id_reuniao`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
