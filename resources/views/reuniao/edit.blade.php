@extends ("template.app")

@section("content")
<div class="col-md-12">
    <h3>Editar <strong> {{ $reuniao['nome'] }} </strong></h3>
</div>
<div class="row">
    <div class="col-md-6 well bg-light rounded-circle">
      <form action="{{ url('reuniaos/update') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $reuniao['id_reuniaos']}}">
          <div class="col-md-12 p-2 {{ $errors->has('nome') ? 'has-errors' : ''}}">
              <label class="control-label">Nome</label>
              <input type="text" value="{{ $reuniao['nome'] }}" name="nome" placeholder="Nome" class="form-control">
              @if($errors->has('nome'))
                <span class="form-text">
                  {{ $errors->first('nome') }}
                </span>
              @endif
          </div>
          <div class="col-md-12 p-2 {{ $errors->has('email') ? 'has-errors' : ''}}">
              <label class="control-label">E-mail</label>
              <input type="text" value="{{ $reuniao['email'] }}"name="email" placeholder="E-mail" class="form-control">
              @if($errors->has('email'))
                <span class="form-text">
                  {{ $errors->first('email') }}
                </span>
              @endif
          </div>
          <div class="col-md-12 p-2 {{ $errors->has('telefone') ? 'has-errors' : ''}}">
              <label class="control-label">Telefone</label>
              <input type="text" value="{{ $reuniao['telefone'] }}"name="telefone" placeholder="Telefone" class="form-control">
              @if($errors->has('telefone'))
                <span class="form-text">
                  {{ $errors->first('telefone') }}
                </span>
              @endif
          </div>
          <button class="btn btn-info float-right mt-3">SALVAR</button>
      </form>
    </div>
    <div class="col-md-4 m-5">
        <div class="card bg-defalt">
            <div class="card-header text-center bg-info"> <strong> {{ $reuniao['nome'] }} </strong></div>
            <div class="card-body">
              <p><strong>E-mail: </strong> {{ $reuniao['email']}}</p>
              <p><strong>Telefone: </strong> {{ $reuniao['telefone'] }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
