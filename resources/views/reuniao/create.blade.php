@extends ("template.app")

@section("content")
    <div class="col-md-12">
        <h3>Cadastrar <strong>Nova Reunião</strong></h3>
    </div>

    <div class="col-md-6 well bg-light rounded-circle">
        <form action="{{ url('pessoas/store') }}" method="POST">
          {{ csrf_field() }}
            <div class="col-md-12 p-2 {{ $errors->has('data') ? 'has-errors' : ''}}">
                <label class="control-label">Data</label>
                <input type="date" name="data" value"{{ old('data') }}" placeholder="Data" class="form-control">
                @if($errors->has('data'))
                  <span class="form-text">
                    {{ $errors->first('data') }}
                  </span>
                @endif
            </div>
            <div class="col-md-12 p-2 {{ $errors->has('horario') ? 'has-errors' : ''}}">
                <label class="control-label">Horário</label>
                <input type="time" name="horario" value"{{ old('horario') }}" placeholder="Horário" class="form-control">
                @if($errors->has('horario'))
                  <span class="form-text">
                    {{ $errors->first('horario') }}
                  </span>
                @endif
            </div>
            <div class="col-md-12 p-2 {{ $errors->has('observacao') ? 'has-errors' : ''}}">
                <label class="control-label">Observação</label>
                <input type="text" name="observacao" value"{{ old('observacao') }}" placeholder="Observação" class="form-control">
                @if($errors->has('observacao'))
                  <span class="form-text">
                    {{ $errors->first('observacao') }}
                  </span>
                @endif
            </div>
            <button class="btn btn-info float-right mt-3">SALVAR</button>
        </form>
      </div>
@endsection
