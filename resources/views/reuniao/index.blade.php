@extends ("template.app")

@section("content")

<div class="row">
    @foreach($reuniao as $reuniao)
        <div class="col-md-4 p-1">
            <div class="card bg-defalt">
                <div class="card-header text-center bg-info"> <strong> {{ $reuniao -> nome }} </strong>
                    <a href="{{ url("/reuniao/$reuniao->id_reuniao/editar") }}" class="btn btn-xs btn-info p-1 float-left">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="{{ url("/reuniao/$reuniao->id_reuniao/excluir") }}" class="btn btn-xs btn-info p-1 float-right">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
                <div class="card-body">
                  <p><strong>E-mail: </strong> {{ $reuniao -> email }}</p>
                  <p><strong>Telefone: </strong> {{ $reuniao -> telefone }}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
