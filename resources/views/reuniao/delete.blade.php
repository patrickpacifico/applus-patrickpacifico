@extends ("template.app")

@section("content")
<div class="row">
    <div class="col-md-6 well">
      <div class="col-md-12">
          <h3 class="mt-5">Deseja excluir <strong> {{ $reuniao['nome'] }} </strong> ?</h3>
          <div class="float-left">
              <a class="btn btn-xs btn-info" href="{{ url("/reuniao") }}">
                  <i class="fas fa-arrow-left"></i>
                  &nbsp;Cancelar
              </a>
              <a class="btn btn-xs btn-danger" href="{{ url("/reuniao/$reuniao->id_reuniao/destroy") }}">
                  <i class="fas fa-user-slash"></i>
                  &nbsp;Excluir
              </a>
          </div>
      </div>
    </div>
    <div class="col-md-4 pt-2">
        <div class="card bg-defalt">
            <div class="card-header text-center bg-warning"> <strong> {{ $reuniao['nome'] }} </strong></div>
            <div class="card-body">
              <p><strong>E-mail: </strong> {{ $reuniao['email']}}</p>
              <p><strong>Telefone: </strong> {{ $reuniao['telefone'] }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
