@extends ("template.app")

@section("content")
    <div class="col-md-12">
        <h3>Cadastrar <strong>Nova Pessoa</strong></h3>
    </div>

    <div class="col-md-6 well bg-light rounded-circle">
        <form action="{{ url('pessoas/store') }}" method="POST">
          {{ csrf_field() }}
            <div class="col-md-12 p-2 {{ $errors->has('nome') ? 'has-errors' : ''}}">
                <label class="control-label">Nome</label>
                <input type="text" name="nome" value"{{ old('nome') }}" placeholder="Nome" class="form-control">
                @if($errors->has('nome'))
                  <span class="form-text">
                    {{ $errors->first('nome') }}
                  </span>
                @endif
            </div>
            <div class="col-md-12 p-2 {{ $errors->has('email') ? 'has-errors' : ''}}">
                <label class="control-label">E-mail</label>
                <input type="text" name="email" value"{{ old('email') }}" placeholder="E-mail" class="form-control">
                @if($errors->has('email'))
                  <span class="form-text">
                    {{ $errors->first('email') }}
                  </span>
                @endif
            </div>
            <div class="col-md-12 p-2 {{ $errors->has('telefone') ? 'has-errors' : ''}}">
                <label class="control-label">Telefone</label>
                <input type="text" name="telefone" value"{{ old('telefone') }}" placeholder="Telefone" class="form-control">
                @if($errors->has('telefone'))
                  <span class="form-text">
                    {{ $errors->first('telefone') }}
                  </span>
                @endif
            </div>
            <button class="btn btn-info float-right mt-3">SALVAR</button>
        </form>
      </div>
@endsection
