@extends ("template.app")

@section("content")

<div class="row">
    <div class="col-sm-12 center pb-1">
        @foreach(range('A', 'Z') as $letra)
            <div class="btn-group ">
                <a href="{{ url("/pessoas/" . $letra) }} " class="btn btn-info">
                    {{ $letra }}
                </a>
            </div>
        @endforeach
    </div>
    <div class="col-sm-12 center pb-1">
    <h1>Filtrado por: <strong>{{ $criterio }}</strong> </h1>
    </div>
    @foreach($pessoas as $pessoa)
        <div class="col-md-4 p-1">
            <div class="card bg-defalt">
                <div class="card-header text-center bg-info"> <strong> {{ $pessoa -> nome }} </strong>
                    <a href="{{ url("/pessoas/$pessoa->id_pessoas/editar") }}" class="btn btn-xs btn-info p-1 float-left">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="{{ url("/pessoas/$pessoa->id_pessoas/excluir") }}" class="btn btn-xs btn-info p-1 float-right">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
                <div class="card-body">
                  <p><strong>E-mail: </strong> {{ $pessoa -> email }}</p>
                  <p><strong>Telefone: </strong> {{ $pessoa -> telefone }}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
