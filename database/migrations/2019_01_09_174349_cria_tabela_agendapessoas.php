<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaAgendapessoas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendapessoas', function (Blueprint $table) {
            $table->increments('id_agenda_pessoas');
            $table->integer('id_pessoas')->unsigned();
            $table->foreign('id_pessoas')->references('id_pessoas')->on('pessoas');
            $table->integer('id_reuniao')->unsigned();
            $table->foreign('id_reuniao')->references('id_reuniao')->on('reuniao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendapessoas');
    }
}
