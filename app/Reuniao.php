<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reuniao extends Model
{
    protected $fillable = [
      'id_reuniao',
      'dt_reuniao',
      'hr_reuniao',
      'observacao'
    ];

    protected $table = 'reuniao';
    protected $primaryKey = 'id_reuniao';
}
