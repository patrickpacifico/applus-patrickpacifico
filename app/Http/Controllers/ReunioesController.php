<?php

namespace App\Http\Controllers;

use App\Reuniao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReunioesController extends Controller
{
  private $reuniao;

  public function __construct()
  {
      $this->reuniao = new Reuniao();
  }

  public function index()
  {
    $list_reuniao = Reuniao::all();
    return view('reuniao.index', [
      'reuniao' => $list_reuniao
    ]);
  }

  public function novoView()
  {
      return view('reuniao.create');
  }

  public function store(Request $request)
  {
      $validacao = $this->validacao($request->all());
      if ($validacao->fails()){
          return redirect()->back()
                  ->withErrors($validacao->errors())
                  ->withInput($request->all());
      }

      Pessoa::create($request->all());
      return redirect('/reuniao')->with('message', 'Reunião criada com sucesso!');
  }

  public function excluirView($id_reuniao)
  {
      return view('reuniao.delete', [
          'reuniao' => $this->getPessoa($id_reuniao)
      ]);
  }

  public function destroy($id_reuniao)
  {
      $this->getReuniao($id_reuniao)->delete();

      return redirect(url('/reuniao'))->with('sucess', 'Reunião excluída!');
  }

  public function editarView($id_reuniao)
  {
      return view('reuniao.edit', [
          'reuniao' => $this->getReuniao($id_reuniao)
      ]);
  }

  public function update(Request $request)
  {
      $validacao = $this->validacao($request->all());
      if ($validacao->fails()){
          return redirect()->back()
                  ->withErrors($validacao->errors())
                  ->withInput($request->all());
      }

      $reuniao = $this->getReuniao($request->id);
      $reuniao->update($request->all());

      return redirect('/reuniao');
  }

    protected function getReuniao($id_reuniao)
  {
       return $this->reuniao->find($id_reuniao);
  }

  private function validacao($data)
  {
      $regras = [
        'data' => 'required',
        'horario' => 'required',
        'observacao' => 'required|min:10'
      ];

      $mensagem = [
        'data.required' => 'Campo data é obrigatório',
        'horario.required' => 'Campo horario é obrigatório',
        'observacao.required' => 'Campo observacao é obrigatório',
        'observacao.min' => 'Crie uma observação maior!'
      ];

      return Validator::make($data, $regras, $mensagem);
  }
}
