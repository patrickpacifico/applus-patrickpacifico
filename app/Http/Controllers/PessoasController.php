<?php

namespace App\Http\Controllers;

use App\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PessoasController extends Controller
{
    private $pessoa;

    public function __construct()
    {
        $this->pessoa = new Pessoa();
    }

    public function index($letra)
    {
      $list_pessoas = Pessoa::indexLetra($letra);
      return view('pessoas.index', [
        'pessoas' => $list_pessoas,
        'criterio' => $letra
      ]);
    }

    public function novoView()
    {
        return view('pessoas.create');
    }

    public function store(Request $request)
    {
        $validacao = $this->validacao($request->all());
        if ($validacao->fails()){
            return redirect()->back()
                    ->withErrors($validacao->errors())
                    ->withInput($request->all());
        }

        Pessoa::create($request->all());
        return redirect('/pessoas')->with('message', 'Pessoa criada com sucesso!');
    }

    public function excluirView($id_pessoas)
    {
        return view('pessoas.delete', [
            'pessoa' => $this->getPessoa($id_pessoas)
        ]);
    }

    public function destroy($id_pessoas)
    {
        $this->getPessoa($id_pessoas)->delete();

        return redirect(url('/pessoas'))->with('sucess', 'Pessoa excluída!');
    }

    public function editarView($id_pessoas)
    {
        return view('pessoas.edit', [
            'pessoa' => $this->getPessoa($id_pessoas)
        ]);
    }

    public function update(Request $request)
    {
        $validacao = $this->validacao($request->all());
        if ($validacao->fails()){
            return redirect()->back()
                    ->withErrors($validacao->errors())
                    ->withInput($request->all());
        }

        $pessoa = $this->getPessoa($request->id);
        $pessoa->update($request->all());

        return redirect('/pessoas');
    }

      protected function getPessoa($id_pessoas)
    {
         return $this->pessoa->find($id_pessoas);
    }

    private function validacao($data)
    {
        $regras = [
          'nome' => 'required|min:3',
          'email' => 'required',
          'telefone' => 'required|min:10'
        ];

        $mensagem = [
          'nome.required' => 'Campo nome é obrigatório',
          'email.required' => 'Campo email é obrigatório',
          'telefone.required' => 'Campo telefone é obrigatório',
          'nome.min' => 'Campo nome deve conter no mínimo 3 letras',
          'telefone.min' => 'Digite um telefone válido "DDD+Número - SOMENTE NÚMEROS"'
        ];

        return Validator::make($data, $regras, $mensagem);
    }
}
