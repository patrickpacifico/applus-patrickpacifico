<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agendapessoas extends Model
{
    protected $fillable [
        'id_agenda_pessoas',
        'id_reuniao',
        'id_pessoas',
    ];

    protected $table = 'agendapessoas';
    protected $primaryKey = 'id_agenda_pessoas';
}
