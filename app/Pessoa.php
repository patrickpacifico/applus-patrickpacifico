<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable = [
      'id_pessoas',
      'nome',
      'email',
      'telefone'
    ];

    protected $table = 'pessoas';
    protected $primaryKey = 'id_pessoas';

    public static function indexLetra ($letra)
    {
        return static::where('nome', 'LIKE', $letra . '%')->get();
    }

}
