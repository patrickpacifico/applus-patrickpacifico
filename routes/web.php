<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'pessoas'], function () {
    Route::get('/', function(){
        return redirect('/pessoas/A');
    });
    Route::get('/novo', 'PessoasController@novoView');
    Route::get('/{id_pessoas}/editar', 'PessoasController@editarView');
    Route::get('/{id_pessoas}/excluir', 'PessoasController@excluirView');
    Route::get('/{id_pessoas}/destroy', 'PessoasController@destroy');
    Route::post('/store', 'PessoasController@store');
    Route::post('/update', 'PessoasController@update');
    Route::get('/{letra}', 'PessoasController@index');
});

Route::group(['prefix' => 'reuniao'], function () {
    Route::get('/', 'ReunioesController@index');
    Route::get('/novo', 'ReunioesController@novoView');
    Route::get('/{id_reuniao}/editar', 'ReunioesController@editarView');
    Route::get('/{id_reuniao}/excluir', 'ReunioesController@excluirView');
    Route::get('/{id_reuniao}/destroy', 'ReunioesController@destroy');
    Route::post('/store', 'ReunioesController@store');
    Route::post('/update', 'ReunioesController@update');
    Route::get('/{letra}', 'ReunioesController@index');
});
